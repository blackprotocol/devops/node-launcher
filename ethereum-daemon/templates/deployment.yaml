apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "ethereum-daemon.fullname" . }}
  labels:
    {{- include "ethereum-daemon.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    type: Recreate
  selector:
    matchLabels:
      {{- include "ethereum-daemon.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "ethereum-daemon.selectorLabels" . | nindent 8 }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}

      {{- if .Values.hostNetwork }}
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      {{- end }}

      serviceAccountName: {{ include "ethereum-daemon.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}

      {{- if eq (include "ethereum-daemon.net" .) "mocknet" }}
      initContainers:
        - name: init-scripts
          image: "{{ .Values.image.scripts.repository }}:{{ .Values.image.scripts.tag }}"
          imagePullPolicy: {{ .Values.image.scripts.pullPolicy }}
          volumeMounts:
            - name: entrypoint
              mountPath: /tmp
          command: [ "sh", "-c", "cp /scripts/* /tmp" ]
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.eth.repository }}:{{ .Values.image.eth.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.eth.pullPolicy }}
          {{- if eq (include "ethereum-daemon.net" .) "mocknet" }}
          command: [ "sh", "-c", "apk add curl && /entrypoint/ethereum-mock.sh" ]
          {{- else if eq (include "ethereum-daemon.net" .) "testnet" }}
          command: [ "sh", "-c", 'geth --ropsten --syncmode fast --cache 4096 --http --http.addr 0.0.0.0 --http.port 8545 --http.api "eth,net,web3,miner,personal,txpool,debug" --http.corsdomain "*" --http.vhosts="*" --whitelist 10920274=0xfd652086d220d506ae5b7cb80fde97d2f3f7028d346cc7d9d384a83d3d638532' ]
          {{- else if eq (include "ethereum-daemon.net" .) "mainnet" }}
          command: [ "sh", "-c", 'geth --syncmode snap --cache 4096 --http --http.addr 0.0.0.0 --http.port 8545 --http.api "eth,net,web3,miner,personal,txpool,debug" --http.corsdomain "*" --http.vhosts="*"' ]
          {{- end }}
          volumeMounts:
            - name: entrypoint
              mountPath: /entrypoint
            - name: scripts
              mountPath: /scripts
            - name: data
              mountPath: /root
          env:
            - name: NET
              value: {{ include "ethereum-daemon.net" . }}
          ports:
            - name: rpc
              containerPort: {{ .Values.service.port }}
              protocol: TCP
          startupProbe:
            failureThreshold: 600
            periodSeconds: 10
            timeoutSeconds: 10
            exec:
              command:
                - /scripts/probe.sh
          livenessProbe:
            failureThreshold: 60
            timeoutSeconds: 10
            periodSeconds: 10
            exec:
              command:
                - /scripts/probe.sh
          readinessProbe:
            timeoutSeconds: 10
            exec:
              command:
                - test
                {{- if eq (include "ethereum-daemon.net" .) "mocknet" }}
                - $(geth --dev attach --exec net.listening) == "true"
                {{- else if eq (include "ethereum-daemon.net" .) "testnet" }}
                - $(geth --ropsten attach --exec net.listening) == "true"
                {{- else if eq (include "ethereum-daemon.net" .) "mainnet" }}
                - $(geth attach --exec net.listening) == "true"
                {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: data
        {{- if and .Values.persistence.enabled (not .Values.persistence.hostPath) }}
          persistentVolumeClaim:
            claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ template "ethereum-daemon.fullname" . }}{{- end }}
        {{- else if and .Values.persistence.enabled .Values.persistence.hostPath }}
          hostPath:
            path: {{ .Values.persistence.hostPath }}
            type: DirectoryOrCreate
        {{- else }}
          emptyDir: {}
        {{- end }}
        - name: entrypoint
          emptyDir: {}
        - name: scripts
          configMap:
            name: {{ include "ethereum-daemon.fullname" . }}-scripts
            defaultMode: 0777
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
